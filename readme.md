Roslib Client
====

This is a wrapper around the roslibjs package aiming to improve shortcomings of the original roslib package by introducing a fluent API as well as some useful error and fault handling mechanisms.

For the official roslibjs repository please head to [https://github.com/RobotWebTools/roslibjs](https://github.com/RobotWebTools/roslibjs)

## Main Features

* Fluent API
* Automatic connection recovery
* Reconnect support for topics

## Documentation

Establishing a reliable connection with ROS is one of the main objectives of this library. It is, also, quite simple and straightforward. Just instantiate the library like so:
```js
import RosClient from "roslibjs-client";

const client = new RosClient({
    url: "ws://192.168.0.11:9090"
});
```

Once you instantiate the client, it will connect to the said URL and stay connected. If the connection drops, it will try to reconnect every 5 seconds. You can change this interval by passing an additional `reconnectInterval` option in milliseconds.

### 1. Listening to Topics

Listening to ROS topics is much more reliable and efficient with this library, because the handlers will tolerate connection drops. If your app gets disconnected from ROS, all the topics you have subscribed to are still stored by the Client and once the connection is back, it will instantly resubscribe to all the topics you want to listen.

You also don't have to worry about subscribing to a certain Topic multiple times. Behind the scenes, roslib-client will always subscribe to a Topic once no matter how many handlers you attach to it.

To start listening to a topic simply do:
```js
const listener = client.topic.subscribe(topicName, messageType, (message) => {
    // Here you can handle the message
});
```

To stop listening to it, simply call `listener.dispose()`

This handler will continue to work even after you lose and gain connectivity.

### 2. Service Calls

To make a service call run the following command:

```js
client.service.call(serviceName, serviceType, payload);
```

This returns a promise that will either resolve with the result, or reject with the error.

## Contributing

If you have access to [Seervision NPM organization](https://www.npmjs.com/settings/seervision/packages), follow instructions from [here](https://docs.npmjs.com/creating-and-publishing-scoped-public-packages) to publish a new version of the package.
