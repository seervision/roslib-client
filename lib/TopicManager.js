import ROSLIB from 'roslib';
import constants from './constants';

export default class TopicManager {
  constructor(connection) {
    this.connection = connection;
    this.registeredTopics = {};

    this.connection.on(constants.EVENT_DISCONNECTED, this.onDisconnected);
    this.connection.on(constants.EVENT_CONNECTED, this.onConnected);
  }

  getSignature = (name, messageType) => `${messageType}/${name}`;

  listen = (ros, name, messageType, signature) => {
    this.registeredTopics[signature].listener = new ROSLIB.Topic({
      ros,
      name,
      messageType,
    });
    this.registeredTopics[signature].listener.subscribe((message) => {
      const numHandlers = this.registeredTopics[signature].handlers.length;
      for (let i = 0; i < numHandlers; i++) {
        // Actually invoke topic handlers
        this.registeredTopics[signature].handlers[i](message);
      }
    });
  };

  connectAndListen = (name, messageType, signature) => {
    return this.connection.getInstance().then((ros) => {
      this.listen(ros, name, messageType, signature);
    });
  };

  publish = (name, messageType, payload) => {
    return this.connection.getInstance().then((ros) => {
      const topic = new ROSLIB.Topic({
        ros,
        name,
        messageType,
      });
      const message = new ROSLIB.Message(payload);
      topic.publish(message);
    });
  };

  subscribe = (name, messageType, handler) => {
    const signature = this.getSignature(name, messageType);
    if (signature in this.registeredTopics) {
      // Push to existing handlers
      this.registeredTopics[signature].handlers.push(handler);
    } else {
      // Create handler array and start topic subscription
      this.registeredTopics[signature] = {
        options: { name, messageType },
        listener: undefined,
        handlers: [handler],
      };
      this.connectAndListen(name, messageType, signature);
    }
    return {
      dispose: () => {
        const index = this.registeredTopics[signature].handlers.indexOf(handler);
        if (index !== -1) {
          this.registeredTopics[signature].handlers.splice(index, 1);
          // Close the topic, because no handlers are left
          if (
            !this.registeredTopics[signature].handlers.length &&
            this.registeredTopics[signature].listener
          ) {
            this.registeredTopics[signature].listener.unsubscribe();
            this.registeredTopics[signature].listener = null;
            delete this.registeredTopics[signature];
          }
        }
      },
    };
  };

  onDisconnected = () => {
    for (const topic of Object.values(this.registeredTopics)) {
      if (topic.listener) {
        topic.listener.unsubscribe();
        topic.listener = null;
      }
    }
  };

  onConnected = (ros) => {
    Object.keys(this.registeredTopics).forEach((signature) => {
      const topic = this.registeredTopics[signature];
      if (topic.listener === null && topic.handlers.length) {
        this.listen(ros, topic.options.name, topic.options.messageType, signature);
        topic.listener = null;
      }
    });
  };
}
