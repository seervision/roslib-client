import ROSLIB from 'roslib';

export default class TfClientManager {
  constructor(connection) {
    this.connection = connection;
  }

  get = (options) => {
    return this.connection.getInstance().then((ros) => {
      return new ROSLIB.TFClient({ ...options, ros });
    });
  };
}
