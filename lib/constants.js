export default {
  EVENT_CONNECTED: 'connected',
  EVENT_DISCONNECTED: 'disconnected',
};
