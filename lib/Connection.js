import EventEmitter from 'eventemitter2';
import ROSLIB from 'roslib';

import constants from './constants';

export default class Connection extends EventEmitter {
  constructor(options) {
    super();

    this.connected = false;
    this.connectScheduled = false;
    this.rosInstance = null;

    this.options = options;

    this.connect();
  }

  onFail = () => {
    if (this.connected) {
      this.emit(constants.EVENT_DISCONNECTED);
    }

    this.connected = false;

    if (!this.connectScheduled) {
      this.connectScheduled = true;
      setTimeout(this.connect, this.options.reconnectInterval);
    }
  };

  onSuccess = () => {
    if (this.connected) {
      return;
    }
    this.connected = true;
    this.emit(constants.EVENT_CONNECTED, this.rosInstance);
  };

  connect = () => {
    this.connectScheduled = false;

    this.rosInstance = new ROSLIB.Ros({
      url: this.options.url,
    });
    this.rosInstance.on('close', this.onFail);
    this.rosInstance.on('error', this.onFail);
    this.rosInstance.on('connection', this.onSuccess);
  };

  getInstance = async () => {
    if (this.connected) {
      return this.rosInstance;
    }
    return new Promise((resolve) => this.on(constants.EVENT_CONNECTED, resolve));
  };
}
