import Connection from './Connection';
import ServiceManager from './ServiceManager';
import TopicManager from './TopicManager';
import TfClientManager from './TfClientManager';

export default class Client {
  constructor(options) {
    const connection = new Connection({
      url: 'ws://localhost:9090',
      reconnectInterval: 5 * 1000,
      ...options,
    });
    connection.setMaxListeners(500);

    this.connection = connection;
    this.service = new ServiceManager(connection);
    this.topic = new TopicManager(connection);
    this.tfClient = new TfClientManager(connection);
  }
}
