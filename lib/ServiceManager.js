import ROSLIB from 'roslib';

export default class ServiceManager {
  constructor(connection) {
    this.connection = connection;
  }

  call = (name, serviceType, payload) => {
    return this.connection.getInstance().then((ros) => {
      return new Promise((resolve, reject) => {
        const service = new ROSLIB.Service({
          ros,
          name,
          serviceType,
        });
        const request = new ROSLIB.ServiceRequest(payload);
        service.callService(request, resolve, reject);
      });
    });
  };
}
