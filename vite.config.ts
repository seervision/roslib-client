import { resolve } from "path";
import { defineConfig } from "vite";

export default defineConfig({
  build: {
    lib: {
      entry: "lib/RosLibJsClient.js",
      name: "RosClient",
      // the proper extensions will be added
      fileName: "roslib-client",
    },
    rollupOptions: {
      // make sure to externalize deps that shouldn't be bundled
      // into your library
      external: [],
      output: {},
    },
  },
});
